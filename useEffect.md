## What is useEffect?
useEffect is a React Hook. It is a function inside a component that takes in a function parameter, and an optional dependency list parameter. That function will be run everytime there is an update in the dependency list, or for every render of the component (by omitting the dependency list parameter), or the first time render (by specifying an empty dependency list).

## What is useEffect used for?
It is used for synchronize a component with external systems. For example, a server connection, a database query, an API call to external systems, etc.