import {useState} from 'react';
import logo from './logo.svg';
import './App.css';

function Hello(props) {
    const [name, setName] = useState("")
    return (
        <div>
            Enter your name: {name}
            <br />
            <input type="text" onChange={(e) => setName(e.target.value)} />
            <input type="submit" value="Submit" />
        </div>
    )
}

function Car() {
    const cars = ["Ford", "BMW", "Audi"];
    return (
        <>
            <h1>Who lives in my garage</h1>
            <ul>
                {cars.map((car) => (
                    <li>I am a {car}</li>
                ))}
            </ul>
        </>
    )
}

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Hello />
        <Car />
      </header>
    </div>
  );
}

export default App;
